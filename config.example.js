export const
    /**
     * HTTP server port
     * @type {number}
     */
    port = undefined;