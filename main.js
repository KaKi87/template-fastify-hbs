import { join as joinPath } from 'https://deno.land/std@0.201.0/path/mod.ts';

import createFastify from 'npm:fastify@4.22.2';
import { load as parseYaml } from 'npm:js-yaml@4.1.0';
import fastifyStatic from 'npm:@fastify/static@6.11.0';
import fastifyView from 'npm:@fastify/view@8.0.0';
import Handlebars from 'npm:handlebars@4.7.8';

import {
    port
} from './config.js';

const
    fastify = createFastify({
        logger: true
    }),
    getData = async view => ({
        siteData: parseYaml(await Deno.readTextFile('./data/site.yaml')),
        data: parseYaml(await Deno.readTextFile(`./data/${view}.yaml`))
    });

fastify.register(fastifyStatic, {
    root: joinPath(new URL('.', import.meta.url).pathname, './public'),
    prefix: '/public/'
});

fastify.register(fastifyView, {
    engine: {
        handlebars: Handlebars
    },
    options: {
        partials: {
            'layout': './views/layout.hbs'
        }
    }
});

fastify.get('/', async (request, reply) => reply.view(
    '/views/homepage.hbs',
    await getData('homepage')
));
fastify.get('/:view', async (request, reply) => reply.view(
    `/views/${request.params.view}.hbs`,
    await getData(request.params.view)
));

await fastify.listen({ port });